# ADProsilica conda recipe

Home: "https://github.com/areaDetector/ADProsilica"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADProsilica module
